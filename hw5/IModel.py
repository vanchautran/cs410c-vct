from abc import ABCMeta, abstractmethod

class IModel:
    """
    Fetch all of the entries in the database.
    @params database_name the name of the table or file
    return tuple (# of entries, all entries in the given database)
    """
    __metaclass__ = ABCMeta
    @abstractmethod
    def fetchall(self, database_name):
        pass

    