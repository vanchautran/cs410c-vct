from view_header import Route, PresentView, Flash

class Presenter:
    """docstring for ."""
    def __init__(self, model):
        self.model = model

    # Index
    def index(self):
        """This function returns the home view"""
        return 'home.html'

    #Reviews
    def reviews(self):
        """This function calls the review retrieval method
           and puts it into the routing for presenter to use
        """
        # Temporary placeholder for DB
        reviews = self.model.fetchall('reviews')
        if reviews[0]:
            args = {'reviews':reviews[1]}
            route = Route(False, 'reviews.html', args)
            return PresentView(route)
        else:
            msg = 'No review found'
            args = {'msg':msg}
            route = Route(False, 'reviews.html', args)
            return PresentView(route)