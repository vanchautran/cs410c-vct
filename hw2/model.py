import json
from IModel import IModel

class AppModel(IModel):
    """This is the model class that interacts with the data.
    :param app: The Flask application
    :param name: The app name
    """
    def __init__(self, app, name='course_reviews'):
        self.arg = app

    # Fetch all reviews
    def fetchall(self, database_name):
        """This function fetches the reviews.
           Currently uses hardcoded values.
        :param database_name: The database to fetch the data from
        """
        #reviews = json.load(open('reviews.json'))
        reviews = {
            1: {
                "courseDepartment": "CS",
                "courseID": 410,
                "term": "Spring",
                "year": "2018",
                "instructor": "Wu-Cheng Feng",
                "rating": 4.1,
                "review": "Lots of lectures"
            },
            2: {
                "courseDepartment": "CS",
                "courseID": 498,
                "term": "Spring",
                "year": "2018",
                "instructor": "Suresh Singh",
                "rating": 3.0,
                "review": "Bring coffee to class"
            },
            3: {
                "courseDepartment": "MTH",
                "courseID": 410,
                "term": "Spring",
                "year": "2018",
                "instructor": "John Caughman",
                "rating": 4.8,
                "review": "Almost made me enjoy math"
            },
            4: {
                "courseDepartment": "CS",
                "courseID": 470,
                "term": "Spring",
                "year": "2018",
                "instructor": "Warren Harrison",
                "rating": 3.0,
                "review": "Lectures are long and useless"
            }
        }
        return(len(reviews), reviews)
