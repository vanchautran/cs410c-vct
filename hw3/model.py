import json
from IModel import IModel

import sqlite3
DB_FILE = 'reviews.db'

class AppModel(IModel):
    """This is the model class that interacts with the data.
    :param app: The Flask application
    :param name: The app name
    """
    def __init__(self, app, name='course_reviews'):
        self.arg = app
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from course_reviews")
        except sqlite3.OperationalError:
            cursor.execute("create table course_reviews (coursedep text, courseid integer, term text, year integer, instructor text, rating integer, review)")
        cursor.close()

    # Fetch all reviews
    def fetchall(self, database_name):
        """This function fetches the reviews.
        :param database_name: The database to fetch the data from
        """
        connection = sqlite3.connect(DB_FILE)
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        query = "SELECT * FROM {0}".format(database_name)
        result = cursor.execute(query)
        data = cursor.fetchall()
        cursor.close()
        return(result, data)

    def add_review(self, values):
        """This function adds a new review.
        :param values: The content of the new review
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("INSERT INTO course_reviews VALUES (?, ?, ?, ?, ?, ?, ?)", values)
        connection.commit()
        cursor.close()
        return True
