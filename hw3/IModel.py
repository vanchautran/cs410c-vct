from abc import ABC, abstractmethod

class IModel(ABC):
    """
    Fetch all of the entries in the database.
    @params database_name the name of the table or file
    return tuple (# of entries, all entries in the given database)
    """
    @abstractmethod
    def fetchall(self, database_name):
        pass

    """
    Add a review into the table.
    @params values all the fields needed for adding a review
    """
    @abstractmethod
    def add_review(self, review):
        pass