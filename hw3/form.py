from wtforms import Form, StringField, TextAreaField, IntegerField, validators

# Review form
class ReviewForm(Form):
    courseDepartment = StringField('Course Department', [validators.Length(min=1, max=4)])
    courseID = IntegerField('Course ID', [validators.NumberRange(min=1, max=9999)])
    term = StringField('Term', [validators.Length(max=6)])
    year = IntegerField('Year', [validators.NumberRange(min=1980, max=2018)])
    instructor = StringField('Instructor', [validators.Length(min=1, max=40)])
    rating = IntegerField('Rating', [validators.NumberRange(min=1, max=5)])
    review = TextAreaField('Review', [validators.Length(min=10)])
    