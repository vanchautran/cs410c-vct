from view_header import Route, PresentView, Flash
from flask import session, logging, request

class Presenter:
    """docstring for ."""
    def __init__(self, model):
        self.model = model

    # Index
    def index(self):
        """This function returns the home view"""
        return 'home.html'

    #Reviews
    def reviews(self):
        """This function calls the review retrieval method
           and puts it into the routing for presenter to use
        """
        # Temporary placeholder for DB
        reviews = self.model.fetchall('course_reviews')
        if reviews[0]:
            args = {'reviews':reviews[1]}
            route = Route(False, 'reviews.html', args)
            return PresentView(route)
        else:
            msg = 'No review found'
            args = {'msg':msg}
            route = Route(False, 'reviews.html', args)
            return PresentView(route)

    # Add article
    def add_review(self, can_add, form):
        """This function retrieves data from the form
           and passes the data for the model to add.
        :params form: The form containing new review content
        """
        if can_add:
            courseDepartment = form.courseDepartment.data
            courseID = form.courseID.data
            term = form.term.data
            year = form.year.data
            instructor = form.instructor.data
            rating = form.rating.data
            review = form.review.data
            self.model.add_review((courseDepartment, courseID, term, year, instructor, rating, review))
            route = Route(True, 'reviews')
            return PresentView(route)
        args = {'form':form}
        route = Route(False, 'add_review.html', args)
        return PresentView(route)